app.controller('UsuarioListaCtrl', function($scope) {  // ~ class

    $scope.usuarios = [
        {
            "id" : 1,
            "nome" : "Fulano",
            "email" : "fulano@email.com",
            "status" : true,
            "endereco" : {
                "rua" : "Rua Fernandópolis",
                "numero" : 2510
            }
        },
        {
            "id" : 2,
            "nome" : "Ciclano",
            "email" : "ciclano@email.com",
            "status" : false,
            "endereco" : {
                "rua" : "Rua Fernandópolis",
                "numero" : 2510
            }
        }
    ]

});